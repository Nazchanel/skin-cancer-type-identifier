# Skin Cancer Type Detection
Takes an inputted picture of skin cancer and detects what type of skin cancer it is.

Link to Dataset: [https://www.kaggle.com/datasets/jaiahuja/skin-cancer-detection](https://www.kaggle.com/datasets/jaiahuja/skin-cancer-detection)